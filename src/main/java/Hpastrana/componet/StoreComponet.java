package Hpastrana.componet;

import net.serenitybdd.screenplay.targets.Target;

public class StoreComponet {
	private StoreComponet() {
		throw new IllegalStateException("Targets class");
	}

	public static final Target OPTION_LAPTOPS = Target.the("option product {0}")
			.locatedBy("//a[text()='{0}']"); 
	public static final Target LIST_LAPTOPS = Target.the("List laptops")
			.locatedBy("//div[@id='tbodyid']//div[@class='card h-100']//div[@class='card-block']"); 
	public static final Target BUTTON_PREVIEW = Target.the("button preview")
			.locatedBy("//button[text()='Previous']"); 
	
}
