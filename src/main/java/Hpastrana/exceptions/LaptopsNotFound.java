package Hpastrana.exceptions;

public class LaptopsNotFound extends AssertionError{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LaptopsNotFound(String message, Throwable cause) {
		super(message, cause);
	}
}
