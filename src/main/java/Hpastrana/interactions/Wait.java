package Hpastrana.interactions;



import net.serenitybdd.core.time.InternalSystemClock;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Wait implements Task{
	private int time;
	public Wait(int time) {
		this.time=time;
	}
	@Override
	public <T extends Actor> void performAs(T actor) {
		new InternalSystemClock().pauseFor(time);
		
	}
	public static Wait forA(int time) {
		return Tasks.instrumented(Wait.class, time);
	}
	
	public Wait seconds() {
		this.time=time*1000;
		return this;
	}
}
