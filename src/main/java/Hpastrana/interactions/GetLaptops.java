package Hpastrana.interactions;

import java.util.ArrayList;
import java.util.List;

import Hpastrana.componet.StoreComponet;
import Hpastrana.questions.GetList;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class GetLaptops implements Interaction {

	@Override
	public <T extends Actor> void performAs(T actor) {
		List<String> listProduct = new ArrayList<>();
		for (int i = 0; i < StoreComponet.LIST_LAPTOPS.resolveAllFor(actor).size(); i++) {
			listProduct.add(actor.asksFor(GetList.product(i)));
		}
		actor.remember("listProduct", listProduct);
	}

	public static GetLaptops current() {
		return new GetLaptops();
	}
}
