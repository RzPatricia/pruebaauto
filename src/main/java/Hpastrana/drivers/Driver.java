package Hpastrana.drivers;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Driver {
	private static WebDriver driverWeb;

	public static Driver chrome() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("--ignore-certificate-errors");
		driverWeb = new ChromeDriver(options);
		driverWeb.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		return new Driver();
	}
	
	public  WebDriver pagina() {
		driverWeb.get("https://www.demoblaze.com/index.html");
		return driverWeb;
	}
	 
	
	public static Driver getDriver() {
		return new Driver();
	}
	
	 
}
