package Hpastrana.tasks;

import Hpastrana.componet.StoreComponet;
import Hpastrana.interactions.GetLaptops;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class SelectThe implements Task {

	private String opcion;

	public SelectThe(String opcion) {
		this.opcion = opcion;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(StoreComponet.OPTION_LAPTOPS.of(opcion)),
				WaitUntil.the(StoreComponet.LIST_LAPTOPS, WebElementStateMatchers.isPresent()), GetLaptops.current());
	}

	public static SelectThe menu(String opcion) {
		return new SelectThe(opcion);
	}

}
