package Hpastrana.tasks;

import Hpastrana.componet.StoreComponet;
import Hpastrana.interactions.GetLaptops;
import Hpastrana.interactions.Wait;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class SelectPage implements Task{

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(StoreComponet.BUTTON_PREVIEW),Wait.forA(5).seconds(),GetLaptops.current());
				 
	}
	public static SelectPage preview() {
		return new SelectPage();
	}

}
