package Hpastrana.questions;

import java.util.List;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ViewProduct implements Question<Boolean> {

	@Override
	public Boolean answeredBy(Actor actor) {
		List<String> listProduct = actor.recall("listProduct");
		int cont = actor.asksFor(GetMatch.fromList());
		boolean result = false;

		if (cont == listProduct.size()) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	public static ViewProduct correctly() {
		return new ViewProduct();
	}

}
