package Hpastrana.questions;

import Hpastrana.componet.StoreComponet;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class GetList implements Question<String> {

	private int position;

	public GetList(int position) {
		this.position = position;
	}

	@Override
	public String answeredBy(Actor actor) {
		String listProduct = StoreComponet.LIST_LAPTOPS.resolveAllFor(actor).get(position).getText() + "||";
		return listProduct;
	}

	public static GetList product(int position) {
		return new GetList(position);
	}

}
