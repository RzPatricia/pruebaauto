package Hpastrana.questions;

import java.util.List;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class GetMatch implements Question<Integer> {

	@Override
	public Integer answeredBy(Actor actor) {
		List<String> listProduct = actor.recall("listProduct");
		int cont = 0;
		for (int i = 0; i < listProduct.size(); i++) {
			if ((listProduct.get(i).contains("Core i7")) || (listProduct.get(i).contains("laptop"))
					|| (listProduct.get(i).contains("MacBook"))) {
				cont++;
			}

		}
		return cont;
	}

	public static GetMatch fromList() {
		return new GetMatch();
	}

}
