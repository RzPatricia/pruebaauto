package Hpastrana.definitions;

import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import Hpastrana.drivers.Driver;
import Hpastrana.exceptions.LaptopsNotFound;
import Hpastrana.questions.ViewProduct;
import Hpastrana.tasks.SelectPage;
import Hpastrana.tasks.SelectThe;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;

public class ConsultarLaptosDefinitions {
	@Before
	public void setStage() {
		setTheStage(Cast.ofStandardActors());
	}

	@Dado("^que el (.*) ingresa a la pagina demoblaze$")
	public void queElClienteIngresaALaPaginaDemoblaze(String usuario) {
		theActorCalled(usuario).can(BrowseTheWeb.with(Driver.chrome().pagina()));

	}

	@Cuando("^ingresa a la seccion (.*)$")
	public void ingresaALaSeccion(String opcion) {
		theActorInTheSpotlight().attemptsTo(SelectThe.menu(opcion));
	}

	@Entonces("^el cliente visualiza el listado de laptops$")
	public void elClienteVisualizaElListadoDeLaptops() {
		theActorInTheSpotlight()
				.should(GivenWhenThen.seeThat(ViewProduct.correctly())
						.orComplainWith(LaptopsNotFound.class));
	}
	@Cuando("^navega entre la opcion de paginacion$")
	public void navegaEntreLaOpcionDePaginacion() {
		theActorInTheSpotlight().attemptsTo(SelectPage.preview());
	}


}
