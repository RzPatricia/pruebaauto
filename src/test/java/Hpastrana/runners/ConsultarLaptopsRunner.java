package Hpastrana.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/Hpastrana/features/consultar_laptops.feature",
snippets = SnippetType.CAMELCASE, 
glue = "Hpastrana.definitions" 
//tags = "@ListarProductosPaginacion"
)
public class ConsultarLaptopsRunner {

}
