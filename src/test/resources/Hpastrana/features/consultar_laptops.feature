#language:es
Característica: Yo como cliente del almacén demoblaze necesito visualizar los precios de los productos de la categoría Laptops
  para tomar decisiones de compra

  @ListarProductos
  Escenario: Listar laptops
    Dado que el cliente ingresa a la pagina demoblaze
    Cuando ingresa a la seccion Laptops
    Entonces el cliente visualiza el listado de laptops

  @ListarProductosPaginacion
  Escenario: Consultar laptops segun la paginacion
    Dado que el cliente ingresa a la pagina demoblaze
    Cuando ingresa a la seccion Laptops
    Y navega entre la opcion de paginacion
    Entonces el cliente visualiza el listado de laptops
